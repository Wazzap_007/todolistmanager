﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ToDoListManager
{
	public class TaskItemsList: INotifyPropertyChanged
	{
		private ObservableCollection<TaskItem> tasks;
		public ObservableCollection<TaskItem> Tasks
		{
			get
			{
				return tasks;
			}
			set
			{
				if (tasks != value)
				{
					tasks = value;
					NotifyPropertyChanged(nameof(Tasks));
				}
			}
		}

		public string NewTaskName
		{
			get;
			set;
		}

		public ICommand CreateTaskCommand
		{
			get
			{
				return new CreateTaskCommand();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged(string name)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
		}
	}
}

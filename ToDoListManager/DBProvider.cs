﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToDoListManager;

namespace ToDoListManager
{
	public class DBProvider
	{
		private static string connectionString;

		public static ObservableCollection<TaskItem> LoadList()
		{
			if (String.IsNullOrEmpty(connectionString))
			{
				ChangeConnectionString();
			}

			using (IDbConnection connection = new SQLiteConnection(connectionString))
			{
				var output = connection.Query<TaskItem>("Select * from Task", new DynamicParameters());
				ObservableCollection<TaskItem> col = new ObservableCollection<TaskItem>(output);
				return col;
			}
		}

		public static bool SaveTask(TaskItem newItem)
		{
			try
			{
				if (String.IsNullOrEmpty(connectionString))
				{
					ChangeConnectionString();
				}

				using (IDbConnection connection = new SQLiteConnection(connectionString))
				{
					connection.Execute("Insert into Task (Name, IsDone, Date) values (@Name, @IsDone, @Date)", newItem);
				}
				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Oops!");
				return false;
			}
		}

		public static void ChangeConnectionString(string DbId = "Default")
		{
			connectionString = ConfigurationManager.ConnectionStrings[DbId].ConnectionString;
		}
	}
}

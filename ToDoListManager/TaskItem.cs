﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoListManager
{
	public class TaskItem
	{
		public string Name
		{
			get;
			set;
		}

		public bool IsDone
		{
			get;
			set;
		}

		public string Date
		{
			get;
			set;
		}
	}
}

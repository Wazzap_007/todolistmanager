﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ToDoListManager
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow: Window
	{
		public ObservableCollection<TaskItem> Tasks
		{
			get;
			set;
		}

		TaskItemsList tasks;

		public MainWindow()
		{
			InitializeComponent();

			tasks = new TaskItemsList
			{
				Tasks = DBProvider.LoadList()
			};

			this.DataContext = tasks;
		}

		private void Go_Click(object sender, RoutedEventArgs e)
		{
			//Calendar.SelectedDate.Value.Date.ToShortDateString();

		}
	}
}

/*
 selectedDate.Value.Date.ToShortDateString()
	 */

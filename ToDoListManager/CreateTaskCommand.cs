﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ToDoListManager
{
	public class CreateTaskCommand: ICommand
	{
		public event EventHandler CanExecuteChanged;

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			if (parameter is TaskItemsList taskList)
			{
				var newTask = new TaskItem()
				{
					Name = taskList.NewTaskName,
					IsDone = false,
					Date = DateTime.Now.AddDays(1).ToShortDateString()
				};

				if (DBProvider.SaveTask(newTask))
					taskList.Tasks.Add(newTask);
			}
		}
	}
}
